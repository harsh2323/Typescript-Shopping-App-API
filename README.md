# Typescript Shopping App

  

This is a simple API to get the data for products.

Allowed methods

- GET

- POST

- DELETE

  

This will be implemented in a future shopping application( soon )

  

# Usage

  

You need some dependencies to test this API

- Express
-  Typescript
-  MongoDB
-  Mongoose

To install these dependencies locally simply clone this repo and run the script

``` npm install```

This will install everything you need

Now, simply type this command to test the API locally

``` npm run start ```

you will see that the api is running locally on ```localhost:3000```, so go to your browser and type ```localhost:3000``` as your URL.

Here are the endpoints currently available 

- /api
- /api/products-name

putting these endpoints in your URL will give you the data that you are looking for.

More to come soon!!
